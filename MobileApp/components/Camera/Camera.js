'use strict';

import React, {Component} from 'react';
import {StyleSheet, Text, TouchableHighlight, View} from 'react-native';
import {Container} from 'native-base';
import Camera from 'react-native-camera';
import {observable, toJS} from 'mobx';
import {observer} from 'mobx-react';

@observer
export default class Camera_Helper extends Component {
    @observable cameraType = "back";
    render() {
        return (
            <Container key={this.props.navigation.state.params.index}>
                <Camera
                    ref={(cam) => {
                        this.camera = cam;
                    }}
                    type={this.cameraType}
                    style={styles.preview}>
                    <View style={styles.buttonBar}>
                        <TouchableHighlight 
                            style={styles.button} 
                            onPress={this._switchCamera.bind(this)}>
                            <Text style={styles.buttonText}>Flip</Text>
                        </TouchableHighlight>
                        <TouchableHighlight 
                            style={styles.button} 
                            onPress={this._takePicture.bind(this)}>
                            <Text style={styles.buttonText}>Take</Text>
                        </TouchableHighlight>
                    </View>
                </Camera>
            </Container>
        );
    }

    _takePicture() {
        const options = {};
        //options.location = ...
        this.camera.capture({metadata: options})
        .then((data) => {
            console.log(data);
            this.props.navigation.state.params.pictureTaken();
            this.props.navigation.state.params.onAnswer(this.props.navigation.state.params.index, data);
            this.props.navigation.navigate('CameraRoll', {formTitle: this.props.navigation.state.params.formTitle, docID: this.props.navigation.state.params.docID});
        })
        .catch(err => console.error(err));
    }
    _switchCamera() {
        console.log("[Camera] -> type: " + this.cameraType);
        if(this.cameraType == "back")
            this.cameraType = "front";
        else
            this.cameraType = "back";
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  },
//   container: {
//         flex: 1,
//         justifyContent: "center",
//         alignItems: "center",
//         backgroundColor: "transparent",
//     },
    buttonBar: {
        flexDirection: "row",
        position: "absolute",
        bottom: 25,
        right: 0,
        left: 0,
        justifyContent: "center"
    },
    button: {
        padding: 10,
        //color: "#000",
        borderWidth: 1,
        borderColor: "#fff",
        margin: 5
    },
    buttonText: {
        color: "#fff"
    }
});