'use strict';

import React, {Component} from 'react';
import { AppNavigator } from './components/AppNavigator';
import FormDB from './FormDB.js';
import Common_Data from './components/Forms/Common_Data';
import { Provider } from "mobx-react";
import imageData from './components/Camera/ImageURIs';
// import Login from './components/Login/Login';
// import {observer, observable} from 'mobx'
// import Camera from 'react-native-camera'

const formDB = new FormDB();
const commonData = new Common_Data();
const imagedata = new imageData();

export default class App extends Component {
  render() {
    return (
        <Provider formDB={formDB} commonData={commonData} imagedata={imagedata}>
            <AppNavigator/>
        </Provider>

    );
  }
}