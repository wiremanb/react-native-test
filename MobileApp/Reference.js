/**
 * Created by bbanks.int on 5/22/2017.
 *
 * Used as a static reference class for constants
 */

export default class Reference {

    static DBTypes = {
        form: "form",
        formTemplate: "formTemplate",
        questionTemplate: "questionTemplate",
        category: "category"
    };

    static formBuilderModes = {
        formBuilder: "formBuilder",
        templateBuilder: "templateBuilder",
        formEntry: "formFillerOuter"
    };

    static formTemplateStartKey = "formTemplate_";

    static formStartKey = "form_";

    static templateStartKey = "questionTemplate_";

    static formQueryLimit = 100;

    static QuestionTypes = {
        QuestionSet: "Question Set",
        LineEdit: "Line Edit",
        MultiLineEdit: "Multi-line Edit",
        YesNo: "Yes or No",
        AutoComplete: "Pick List",
        Date: "Date",
        Picture: "Picture",
        GPS: "GPS"
    };
}