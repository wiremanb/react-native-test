import React, { Component } from "react";
import { View} from 'react-native'
import { Container, Content, Item, Input } from 'native-base';
import {observable, toJS} from 'mobx';
import {observer} from 'mobx-react';

@observer
export default class LineEdit extends Component {
    @observable value = "";

    constructor(props) {
        super(props);
    };

    componentWillMount() {
        if(this.props.answer != null)
            this.value = this.props.answer;
    }

    handleChange = (event) => {
        // Do something with text here
        // Can access text through event.nativeEvent.text
        // or this.state.value

        //console.log(event.nativeEvent.text)
        this.value = event.nativeEvent.text;
        this.props.onAnswer(this.props.index, this.value);

        // this.setState({value: event.nativeEvent.text});
        //If we passed a callback, call it

        if(this.props.onUpdateInput)
           this.props.onUpdateInput(event);
    };

    render() {

        return (
			<View >
				<Input
					style={{borderColor: 'gray', borderWidth: 1, width: 250, backgroundColor:"white", marginRight: 20}}
					multiline={this.props.multiline}
					placeholder={this.props.placeholder}
					value={this.props.type === "autocomplete" ? this.props.value : this.value}
					onChange={this.handleChange}
					onFocus={this.props.onFocus}
					onBlur={this.props.onBlur}
				/>
            </View>
        );
    }
}