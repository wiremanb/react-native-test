'use strict';

import React, { Component } from 'react';
import {ListView, Text, View} from 'react-native';
import Native, {Alert} from 'react-native';
import {NavigationActions} from 'react-navigation';
import styles from './styles.js'
import {Container, Content, List, ListItem, Button, Icon, Badge, Left, Right, Center, Fab} from 'native-base';
import {observable, computed} from 'mobx';
import {observer, inject} from 'mobx-react';

@inject("formDB")
@observer
class MainMenu extends Component {
	@observable datasource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2, });
	@observable tasksToDo = 0;
	@observable tasksComplete = 0;
	@observable questionsToAnswer = 0;
	@observable firstTime = true;
	TO_ID = 0;
	INT_ID = 0;

	static navigationOptions = ({ navigation, screenProps }) => ({
		title: 'Tasks',
		headerRight: <Text style={{marginRight: 10, fontSize:20}}>{`${navigation.state.params.username}`}</Text>,
		headerLeft: <Native.Button title='Logout' onPress={() => {
			const resetAction = NavigationActions.reset({
				index: 0,
				key: null,
				actions: [NavigationActions.navigate({ routeName: 'Login' })],
			});

			Alert.alert(
				'Confirmation Required',
				'Do you really want to logout?',
				[
					{text: 'Accept', onPress: () => {navigation.dispatch(resetAction);}},
					{text: 'Cancel'}
				]
			);
		}}/>,
	});

	constructor(props) {
		super(props);
		console.ignoredYellowBox = [
			'Setting a timer'
		];
		this.TO_ID = setTimeout(this._refreshData, 250);
		this.INT_ID = setInterval(this._refreshData, 1000);
	}

	@computed
	get questionCount() {
		return this.questionsToAnswer;
	}

	_setQuestionCount(rowdata) {
		if(rowdata != null) {
			this.questionsToAnswer = rowdata.childCount;
		} else this.questionsToAnswer = 0;
	}

	componentDidMount() {
		// this._refreshData();
		// console.log("mounted");
		// this.TO_ID = setTimeout(this._refreshData, 250);
		// this.INT_ID = setInterval(this._refreshData, 30000);
	}

	componentWillUnmount() {
		console.log("unmounting...");
		clearTimeout(this.TO_ID);
		clearImmediate(this.INT_ID);
	}

	_refreshData = async () => {
		try {
			// this.props.formDB.loadClean();
			let tmp = [];
			tmp = await this.props.formDB.formTemplates.map(form => form);
			// console.log("tmp.length: " + tmp.length);
			if(tmp.length > 0)
				this.datasource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.id !== r2.id}).cloneWithRows(tmp);
		} catch(e) {
			console.log("_refreshData failed: " + e.message);
		}
	};


	_renderRow = (rowdata) => {
		this._setQuestionCount(rowdata);
		try {
			return (
				<Button style={{marginTop: 20, margin: 10,  backgroundColor:"#EEECE5"}} primary block
						onPress={() => {
							// clearTimeout(this.TO_ID);
							// clearImmediate(this.INT_ID);
							this.props.navigation.navigate('Form', {
								title: rowdata.title,
								docID: rowdata.id
							})}}>
					<Text style={{textAlign: 'center', fontSize: 20}}> {rowdata.title} </Text>
					<Badge width={27} success={this.questionCount===0} style={{justifyContent: 'center', alignItems: 'center', left: 10, top: 1}}>
						<Text>{this.questionCount}</Text>
					</Badge>
				</Button>
			);
		}
		catch(e) {
			return null;
		}
	};

	render() {

		return (
			<Container style={styles.container}>
				<Content>
					<ListView
						enableEmptySections={true}
						dataSource={this.datasource}
						renderRow={this._renderRow}/>
				</Content>
				<Fab style={{margin: 10}}
					 block bordered info
					 onPress={() => {
						 this._saveForm()
					 }}>
					<Icon name="refresh"/>
					{/*<Text style={{fontSize: 20}}> Save </Text>*/}
				</Fab>
			</Container>
		);
	}
}

export default MainMenu;