import React from 'react';
import {observable} from 'mobx';
import {observer} from 'mobx-react';

@observer
export default class Common_Data {
    @observable forms;
    @observable categories;
    @observable formData;
    @observable currentForm;
    @observable answeredForm;
    @observable.ref formDataHistory = [];
    @observable latitude = null;
    @observable longitude = null;
    @observable gotCoords = false;

}