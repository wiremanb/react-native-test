/**
 * Created by jdeep on 6/13/2017.
 */

import { observable, toJS } from "mobx";
import Reference from "./Reference.js";
import { pouchDB } from "./Pouch_Helper";

export default class FormDB {
    @observable DatabaseInitialized = true;
    @observable.ref categories = []; // all categories for forms
    @observable.ref formTemplates = []; // form templates used for building form
    @observable.ref questionTemplates = []; // question set templates used for building form
    @observable currentForm = {}; // current form in form builder
    @observable username = "";

    auth = async (username, password) => {
        this.username = username;
        return await pouchDB.auth(username, password, this.update);
    };

    update = async () => {
        if (!this.DatabaseInitialized) {
            await this.initializeDB();
        }

        this.categories = [];
        this.questionTemplates = [];
        this.allSavedForms = [];

        try {

            this.categories = toJS(
                (await pouchDB.get(Reference.DBTypes.category)).categories
            );

            this.formTemplates = this.sanitize(
                (await pouchDB.query(this.mapFormTemplates, {include_docs: false})).rows
            );
            this.questionTemplates = this.sanitize(
                (await pouchDB.query(this.mapQuestionTemplates, {include_docs: false}))
                    .rows
            );
        } catch(e) {
            console.log("error in update: " + e.message);
        }
    };

    //region categories processing
    addCategory = async newCategory => {
        const categories = await pouchDB.get(Reference.DBTypes.category);
        categories.categories.push(newCategory);
        this.categories = categories.categories;
        await pouchDB.put(categories);
    };

    updateCategory = async (nameCurrently, newName) => {
        for (let i = 0; i < this.categories.length; i++) {
            if (this.categories[i] === nameCurrently) {
                this.categories[i] = newName;
            }
        }

        const categories = await pouchDB.get(Reference.DBTypes.category);
        categories.categories = this.categories;
        await pouchDB.put(categories);
    };

    removeCategory = async categoryName => {
        const index = this.categories.indexOf(categoryName);

        if (index !== -1) {
            this.categories.splice(index, 1);

            const categories = await pouchDB.get(Reference.DBTypes.category);
            categories.categories = this.categories;

            await pouchDB.put(categories);
        }
    };

    saveForm = async (form) => {
        let doc = await pouchDB.get(form._id);
        if(doc) {
            doc._id = form._id;
            //doc._rev = form._rev;
            doc.title = form.title;
            doc.type = form.type;
            doc.createdBy = form.createdBy;
            doc.createdOn = form.createdOn;
            doc.assignedTo = form.assignedTo;
            doc.category = form.category;
            doc.formData = form.formData;
            await pouchDB.put(doc);
        }
        else {
            await pouchDB.put(form);
        }
    };

    addAttachment = async (docID, attachmentID, data) => {
        await pouchDB.putAttachment(docID, attachmentID, data);
    };

    getAttachment = async (docID, attachmentID) => {
        return await pouchDB.getAttachment(docID, attachmentID);
    };

    removeAttachment = async (docID, attachmentID) => {
        await pouchDB.removeAttachment(docID, attachmentID);
    };

    getAllDocs = async () => {
        return await pouchDB.getAllDocs();
    };

    loadClean = async () => {
        await this.update();
        // this.clearCurrentForm();
    };

    initializeDB = async () => {
        const categories = {
            _id: Reference.DBTypes.category,
            type: Reference.DBTypes.category,
            categories: ["Site Surveys", "Commissioning"]
        };
        await pouchDB.put(categories);
    };

    submitForm = async (form) => {
        await pouchDB.put(form);
    };

    removeDocument = async id => {
        await pouchDB.remove(id);
    };

    //region auxiliary functions
    getDocument = async id => {
        return await pouchDB.get(id);
    };

    sanitize = docs => {
        return docs.map(doc => doc.value);
    };

    mapFormTemplates = (doc, emit) => {
        if (doc && doc.type && doc.type === Reference.DBTypes.formTemplate && doc.assignedTo === this.username) {
            emit(doc._id, { id: doc._id, title: doc.title, category: doc.category , assignedTo: doc.assignedTo, childCount: doc.formData.length});
        }
    };

    mapQuestionTemplates = (doc, emit) => {
        if (doc && doc.type && doc.type === Reference.DBTypes.questionTemplate) {
            emit(doc._id, { id: doc._id, title: doc.title, category: doc.category });
        }
    };
    //endregion
}