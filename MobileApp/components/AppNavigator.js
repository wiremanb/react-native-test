'use strict';

import React from 'react';
import {StackNavigator, NavigationActions} from 'react-navigation';
import LoginScreen from './Login/Login.js';
import MainMenuScreen from './MainMenu.js';
import CameraScreen from './Camera/Camera.js';
import CameraRollScreen from './Camera/CameraRoll';
import FormScreen from './Forms/Form.js';
import MapScreen from './GPS/maps';

export const AppNavigator = StackNavigator({
    Login: {
        screen: LoginScreen,
        navigationOptions: ({ navigation }) => ({
            title: 'SSI Login',
            // headerRight: <Button title='Logout' onPress={() => navigation.navigate('Login')}/>,
        }),
    },
    MainMenu: {
        screen: MainMenuScreen,
        /*navigationOptions: ({ navigation }) => ({
            title: 'MainMenu',
            headerLeft: <Button title='Logout' onPress={() => {
                const resetAction = NavigationActions.reset({
                    index: 0,
                    key: null,
                    actions: [NavigationActions.navigate({ routeName: 'Login' })],
                });

                Alert.alert(
                    'Confirmation Required',
                    'Do you really want to logout?',
                    [
                        {text: 'Accept', onPress: () => {navigation.dispatch(resetAction);}},
                        {text: 'Cancel'}
                    ]
                );
            }}/>,
        }),*/
    },
    Form: {
        screen: FormScreen,
        // navigationOptions: ({ navigation }) => ({
        //     title: `${navigation.state.params.title}`,
        //     // headerRight: <Button title='Logout' onPress={() => navigation.navigate('Login')}/>,
        // }),
    },
    // QuestionSet: {
    //     screen: QuestionSetScreen,
    //     navigationOptions: ({ navigation }) => ({
    //         title: `${navigation.state.params.title}`,
    //         // headerRight: <Button title='Logout' onPress={() => navigation.navigate('Login')}/>,
    //     }),
    // },
    Camera: {
        screen: CameraScreen,
        navigationOptions: ({ navigation }) => ({
            title: 'Camera',
            // headerRight: <Button title='Logout' onPress={() => navigation.navigate('Login')}/>,
        }),
    },
    CameraRoll: {
        screen: CameraRollScreen,
        // navigationOptions: ({ navigation }) => ({
            // title: 'Pictures',
            // headerRight: <Button title='Logout' onPress={() => navigation.navigate('Login')}/>,
        // }),
    },
    Maps: {
        screen: MapScreen,
        navigationOptions: ({ navigation }) => ({
            title: 'Maps',
            // headerRight: <Button title='Logout' onPress={() => navigation.navigate('Login')}/>,
        }),
    },
});