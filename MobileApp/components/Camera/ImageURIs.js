import {observable} from 'mobx';

export default class imageData {

    @observable uri = [];

    updateURI = async (uri) => {
        console.log("updateURI: " + uri);
        this.uri.push(uri);
        console.log("updateURI array: " + this.uri);
    }

}