'use strict';

import React, { Component } from 'react';
import Native, {Text,ScrollView, View, StyleSheet, ListView, Alert} from 'react-native';
import {Container, Content, Button, Icon, Badge, Fab} from 'native-base';
import Breadcrumb from 'react-native-breadcrumb';
import {observable, computed, toJS} from 'mobx';
import {observer, inject} from 'mobx-react';
import {Question} from './Question';
import {NavigationActions} from "react-navigation";

@inject("formDB", "commonData")
@observer
class Form extends Component {

    currentFormData = null;
    INT_ID = 0;
    questionsToAnswer = 0;
    @observable form = null;
    @observable formData = null;
    @observable formDataHistory = [];
    @observable crumbs = [];
    @observable crumbIndex = 0;
    @observable zIndex = 0;
    @observable newAttachments = [];
    @observable oldAttachments = [];

    // static navigationOptions = ({ navigation, screenProps }) => ({
    //     title: `${navigation.state.params.username}`,
    //     headerRight: <Native.Button title='Submit' onPress={() => {
    //
    //         //
    //         // Alert.alert(
    //         //     'Confirmation Required',
    //         //     'Do you want to submit the data on this form?',
    //         //     [
    //         //         {text: 'Accept', onPress: () => {navigation.dispatch(resetAction);}},
    //         //         {text: 'Cancel'}
    //         //     ]
    //         // );
    //     }}/>,
    // });



    constructor(props) {
        super(props);
        this._refreshData();
        this.crumbs.push(this.props.navigation.state.params.title)
    }

    handleSubmit() {
    }

    @computed
    get questionCount() {
        return this.questionsToAnswer;
    }

    _setQuestionCount(rowdata) {
        if(rowdata != null) {
            let count = 0;
            let data = rowdata.children;
            if(data != null) {
                for (var x = 0; x < data.length; x++) {
                    if (data[x].answer == null && data[x].type != "Question Set")
                        count++;
                }
            }
            this.questionsToAnswer = count;
        } else this.questionsToAnswer = 0;
    }

    _refreshData = async () => {
        try {
            this.form = (await this.props.formDB.getDocument(this.props.navigation.state.params.docID));
            this.formData = this.form.formData;
            this.formDataHistory[0] = this.formData;
        } catch (e) {
            console.log(e);
        }
    };

    _saveForm = async () => {
        try {
            await this.addAttachments(this.newAttachments);
            await this.removeAttachments(this.oldAttachments);
            this.newAttachments = [];
            this.oldAttachments = [];
            await this.props.formDB.saveForm(this.form);
			Alert.alert("Form Saved", "The form has been saved")
        } catch (e) {
            console.log(e.message);
        }
    };

    handleAnswerQuestion = (index, answer) => {
        try {
            //console.log(":::::::::::: " + this.formDataHistory[this.formDataHistory.length-1][index].answer);
            //console.log("------- " + JSON.stringify(this.formData));
            this.formDataHistory[this.formDataHistory.length-1][index].answer = answer;
        } catch(e) {
            console.log("handleAnswerQuestion: " + e.message);
        }
    };

    handleAnswerPictureQuestion = (index, answer, images) => {
        const question = this.formDataHistory[this.formDataHistory.length-1][index];
        const oldAnswer = answer === null ? null : toJS(answer);
        const newAnswer = images === null ? null : toJS(images);

        console.log("OLD: " + JSON.stringify(oldAnswer));
        console.log("NEW: " + JSON.stringify(newAnswer));

        /* get new attachments to be added */
        if (newAnswer !== null && oldAnswer !== null) {
            newAnswer.forEach(newImage => {
                if (oldAnswer === null || !oldAnswer.find(oldImage => oldImage.attachmentID === newImage.attachmentID)) {
                    this.newAttachments.push(Object.assign({}, newImage));
                }
            });
        }

        /* get attachments to be removed */
        if (oldAnswer !== null) {
            oldAnswer.forEach(oldImage => {
                if (
                    !newAnswer.find(
                        newImage => newImage.attachmentID === oldImage.attachmentID
                    )
                ) {
                    this.oldAttachments.push(Object.assign({}, oldImage));
                }
            });
        }

        question.answer = answer;
        this._saveForm();
    }

    addNewAttachment = attachment => {
        console.log("addNewAttachment: "+ attachment)
        this.newAttachments.push(toJS(attachment));
    };

    removeOldAttachment = attachment => {
        this.oldAttachments.push(toJS(attachment));
    };

    addAttachments = async attachments => {
        if (attachments.length > 0) {
            await this.props.formDB.addAttachment(
                this.form._id,
                attachments[0].attachmentID,
                attachments[0].blob
            );
            await this.addAttachments(attachments.slice(1));
        }
    };

    removeAttachments = async attachments => {
        if (attachments.length > 0) {
            await this.props.formDB.removeAttachment(
                this.form._id,
                attachments[0].attachmentID
            );
            await this.removeAttachments(attachments.slice(1));
        }
    };

    loadAttachment = attachmentID => {
        console.log("loadAttachment: " + attachmentID);
        return this.props.formDB.getAttachment(
            this.form._id,
            attachmentID
        );
    };

    renderQuestions = (formData, navigation) => {
        if (formData != null) {
            return (
                formData.map((item, index) => {
                    if (item.type === "Question Set" && item.children != null) {
                        this._setQuestionCount(item);
                        return (
                            <Button primary block
                                    key={JSON.stringify(item) + index}
                                    style={{margin: 10, backgroundColor:"#ddd"}}
                                    onPress={() => {
                                        this.crumbIndex = this.crumbIndex + 1;
                                        this.crumbs.push(item.title);
                                        this.formDataHistory.push(item.children);
                                        //this.renderQuestions(this.formData[index.children], navigation);
                                    }}
									onLongPress={() => {
										Alert.alert(
											'Duplicate',
											item.title,
											[
												{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
												{text: 'Duplicate', onPress: () => {
														var item_copy = Object.assign({}, item);
														formData.splice(index + 1, 0, item_copy);
												}},
											],
											{ cancelable: true }
										);
										//var item_copy = Object.assign({}, item);
										//this.formData.splice(index + 1, 0, item_copy);
									}}
							>

                                <Text style={{paddingLeft:10, fontSize: 20, color: "black"}}> {item.title} </Text>
								<View style={styles.iconStyle} >
                                    <Badge success={this.questionsToAnswer===0} style={{justifyContent: 'center', alignItems: 'center', left: 10, top: 1}}>
                                        <Text style={{color: 'white'}}>{this.questionsToAnswer}</Text>
                                    </Badge>
								    <Icon style={{color:"#0c8aff"}} name="arrow-forward"/>
                                </View>
                            </Button>
                        );
                    }
                    return (
                        <Question
                            key={item.title + index}
                            keyVal={index}
                            type={item.type}
                            navigation={navigation}
                            title={item.title}
                            placeholder={item.hintText}
                            questionText={item.title}
                            values={item.values}
                            answer={item.answer}
                            onAnswer={this.handleAnswerQuestion}
                            addNewAttachment={this.addNewAttachment}
                            removeOldAttachment={this.removeOldAttachment}
                            index={index}
                            docID={this.props.navigation.state.params.docID}
                            page={formData}
                            formTitle={this.props.navigation.state.params.title}
                            loadAttachment={this.loadAttachment}
                            saveForm={this._saveForm}
                        />
                    );
                })
            );
        }
        return null;
    };

    render() {
        if(this.formData != null) {
            if (this.formDataHistory.length >= 1) {
                return (
                    <Container style={{backgroundColor: "white" }}>
                        <ScrollView
                            ref="scrollView"
                            horizontal={true}
                            style={{flex:1, flexDirection:"row", maxHeight: 50,}}
							onContentSizeChange={(contentWidth, contentHeight) => {
								this.refs.scrollView.scrollToEnd({animated: true})
                            }}
                        >
                            <Breadcrumb
                                crumbTextStyle={{fontSize: 20}}
                                entities={toJS(this.crumbs)}
                                isTouchable={true}
                                flowDepth={this.crumbIndex}
                                height={50}
                                onCrumbPress={index => {
                                    this.crumbIndex = index;
                                    this.crumbs.splice(index+1, this.crumbs.length-index);
                                    this.formDataHistory.splice(index+1, this.formDataHistory.length-index);
                                    this.formData = this.formDataHistory[index];
                                }}
                                borderRadius={5}
                            />
                        </ScrollView>
                        <Content disableKBDismissScroll={true} style={{marginTop: 20}}>
                            {this.renderQuestions(this.formDataHistory[this.formDataHistory.length-1], this.props.navigation)}
                        </Content>
						<Fab style={{margin: 10}}
							 block bordered info
							 onPress={() => {
								 this._saveForm()
							 }}>
							<Icon name="cloud-upload"/>
							{/*<Text style={{fontSize: 20}}> Save </Text>*/}
						</Fab>
                    </Container>
                );
            }
            else {
                return (
                    <Container>
                        <Content>
                            {this.renderQuestions(this.formData, this.props.navigation)}
                        </Content>
						<Fab style={{margin: 10}}
							 block bordered info
							 onPress={() => {
								 this._saveForm()
							 }}>
							<Icon name="cloud-upload"/>
							{/*<Text style={{fontSize: 20}}> Save </Text>*/}
						</Fab>
                    </Container>
                );
            }
        }
        else {
            return(
                <Container>
                    <Content>
                        <Text>{"Loading data..."}</Text>
                    </Content>
					<Fab style={{margin: 10}}
						 block bordered info
						 onPress={() => {
							 this._saveForm()
						 }}>
						<Icon name="refresh"/>
						{/*<Text style={{fontSize: 20}}> Save </Text>*/}
					</Fab>
                </Container>
            );
        }
    }
}

const styles = StyleSheet.create({
    questionSet: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 20,
    },

    questionSetBadgeNotComplete: {
        fontSize: 20,
        justifyContent: 'center',
        alignItems: 'center',
        left: 10,
        top: 1,
		flexWrap:"nowrap",
        color: 'red',
    },

    questionSetBadgeComplete: {
        fontSize: 20,
        justifyContent: 'center',
        alignItems: 'center',
        left: 10,
        top: 1,
		flexWrap:"nowrap",
        // color: 'green',
    },
	iconStyle: {
		display:"flex",
		flex: 1,
		flexDirection: 'row',
        alignItems:"center",
		justifyContent: 'flex-end'
	}
});

export default Form;