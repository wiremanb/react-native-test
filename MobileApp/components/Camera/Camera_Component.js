'use strict';

import React, {View, Component} from 'react';
import {Text} from 'react-native';
import {Button, Icon} from 'native-base';
import {observable, computed} from 'mobx';
import {observer} from 'mobx-react';

@observer
export class Camera_Component extends Component {
    @computed
    get picturesTaken() {
        return this.props.answer.length;
    };

    _handelCamera() {
        if(this.picturesTaken === 0)
            this.props.navigate.navigate('CameraRoll', {type: "Camera", saveForm: this.saveForm, loadAttachment: this.props.loadAttachment, answer: this.props.answer, onAnswer: this.props.onAnswer, index: this.props.index, addNewAttachment: this.props.addNewAttachment, removeOldAttachment: this.props.removeOldAttachment, values: this.props.values});
        else
            this.props.navigate.navigate('CameraRoll', {type: "CameraRoll", saveForm: this.saveForm, loadAttachment: this.props.loadAttachment, answer: this.props.answer, onAnswer: this.props.onAnswer, index: this.props.index, addNewAttachment: this.props.addNewAttachment, removeOldAttachment: this.props.removeOldAttachment, values: this.props.values});
    }

    render() {
        return (
            <Button bordered block
                    style={{ margin: 10}}
                    onPress={() => this._handelCamera()}>
                <Icon name="camera"/>
                <Text style={{ margin: 10}}>{"Pictures (" + this.picturesTaken + "/" + this.props.values.max + ")"}</Text>
            </Button>
        );
    }
}