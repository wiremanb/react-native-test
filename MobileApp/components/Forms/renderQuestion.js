'use strict';

import React, { Component } from "react";
import {Question} from './Question';
import {Button, Badge, Text, Icon} from 'native-base';
import {observable} from 'mobx';
import {observer, inject} from 'mobx-react';
import {QuestionSet} from './QuestionSet';
import {Pouch_Helper} from "../../Pouch_Helper";

@inject("commonData")
export class RenderQuestion extends Component{

    renderQuestions = (formData, navigation) => {
        if (formData != null) {
            // this.props.commonData.formDataHistory.push(0);
            return (
                formData.map((item, index) => {
                    if (item.type === "Question Set" && item.children != null) {
                        //     // this._setQuestionCount(item);
                        return (
							<Button bordered primary block
									onPress={() => {
										// console.log("formData: " + JSON.stringify(formData));
										return this.renderQuestions(item.children, navigation)
									}}>
								<Text style={{fontSize: 20}}> {item.title} </Text>
								<Badge width={27} style={{flexWrap:"nowrap"}}>
									<Text>{0}</Text>
								</Badge>
							</Button>
                        );
                    }

                    return (
                        <Question
                            type={item.type}
                            navigation={navigation}
                            title={item.title}
                            placeholder={item.hintText}
                            questionText={item.title}
                            values={['Cat 01', 'Cat 02', 'Cat 03']}/>
                    );
                })
            );
        }
        else return null;
    }
}

export const renderQuestion = new RenderQuestion();