'use strict';

import React from 'react';
import {observable, toJS} from 'mobx';
import {observer, inject} from 'mobx-react';
import { StyleSheet, Text, View } from 'react-native';

export const coordinates = {
	latitude: "",
	longitude: ""
};
@inject("commonData")
@observer
export class GeolocationModule extends React.Component {
	@observable latitude = "";
	@observable longitude = "";
	@observable error = "";
	@observable watchId;

	constructor(props) {
		super(props);
	}

	_updateCoords(position) {
		this.latitude = position.coords.latitude;
		this.longitude = position.coords.longitude;
		this.e = null;
		this.props.commonData.latitude = this.latitude;
		this.props.commonData.longitude = this.longitude;
		this.props.commonData.gotCoords = true;
		console.log("Setting lat and lon")
	}

	_handleError(error) {
		this.e = error.message;
	}

	componentWillMount() {
		this.watchId = navigator.geolocation.watchPosition(
			(position) => {this._updateCoords(position)
			},
			(error) => {this._handleError(error)},
			{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 },
		);
		console.log("WatchId: " + this.watchId);
	}

	componentWillUnmount() {
		navigator.geolocation.clearWatch(this.watchId);
	}

	render() {
		return (null);
	}
}