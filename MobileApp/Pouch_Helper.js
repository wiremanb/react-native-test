/**
 * Created by bbanks.int on 8/8/2017.
 */
import PouchDB from 'pouchdb-react-native';
import React, { Component } from "react";
import config from "./config";

export class Pouch_Helper extends Component {
    DB = new PouchDB("localDB");
    remotedb = null;
    login = "";
    password = "";
    _syncHandler = null;

    auth = (username, password, update) => {
        this.remotedb = new PouchDB(config.CB_ADDRESS, {auth: {username: username, password: password}});
        return new Promise((res, rej) => {
            this.DB.replicate.from(this.remotedb, {
                auth: {username: username, password: password}
            }).on("complete", result => {
                console.log("Replicate result: " + JSON.stringify(result));
                this.login = username;
                this.password = password;
                res(true);
                this.sync(update);
            });
        });
    };

    sync = async (update) => {
        try {
            this._syncHandler = PouchDB.sync(this.DB, this.remotedb, {
                auth: {username: this.login, password: this.password},
                live: true,
                checkpoint: false,
                retry: true
            });
            this._syncHandler.on('change', function(){ update(); console.log("updated")});
            this._syncHandler.on('paused', function(){console.log("paused")});
            this._syncHandler.on('active', function(){console.log("active")});
            this._syncHandler.on('denied', function(){});
            this._syncHandler.on('complete', function(){console.log("complete")});
            this._syncHandler.on('error', function(err){ console.log("Error in sync: " + err); });
        } catch(e) {
            console.log("syncMe error... " + e);
        }
    };

    info = async () => {
        try {
            return await this.DB.info();
        } catch (err) {
            console.log(err);
        }
    };

    put = async obj => {
        try {
            await this.DB.put(obj);
        } catch (err) {
            console.log(err);
        }
    };

    get = async id => {
        try {
            return await this.DB.get(id);
        } catch (err) {
            console.log(err);
            return null;
        }
    };

    query = async (mapFunc, opts) => {
        try {
            return await this.DB.query(mapFunc, opts);
        } catch (err) {
            console.log(err);
        }
    };

    find = async cmd => {
        try {
            return await this.DB.find(cmd);
        } catch (err) {
            console.log(err);
        }
    };

    getAllDocs = async () => {
        try {
            return await this.DB.allDocs({ include_docs: true });
        } catch (err) {
            console.log(err);
        }
    };

    getAllDocsWithStartKey = async (key, limit) => {
        try {
            return await this.DB.allDocs({
                include_docs: true,
                attachments: true,
                limit: limit,
                startkey: key,
                endkey: `${key}\ufff0`
            });
        } catch (err) {
            console.log(err);
        }
    };

    remove = async id => {
        try {
            //needs to get document first to ensure deletes latest revision
            return await this.DB.remove(await this.DB.get(id));
        } catch (err) {
            console.log(err);
        }
    };

    info = async () => {
        try {
            return await this.DB.info();
        } catch (err) {
            console.log(err);
        }
    };

    put = async obj => {
        try {
            await this.DB.put(obj);
        } catch (err) {
            console.log(err);
        }
    };

    get = async id => {
        try {
            return await this.DB.get(id);
        } catch (err) {
            console.log(err);
            return null;
        }
    };

    query = async (mapFunc, opts) => {
        try {
            return await this.DB.query(mapFunc, opts);
        } catch (err) {
            console.log(err);
        }
    };

    find = async cmd => {
        try {
            return await this.DB.find(cmd);
        } catch (err) {
            console.log(err);
        }
    };

    getAllDocs = async () => {
        try {
            return await this.DB.allDocs({ include_docs: true });
        } catch (err) {
            console.log(err);
        }
    };

    getAllDocsWithStartKey = async (key, limit) => {
        try {
            return await this.DB.allDocs({
                include_docs: true,
                attachments: true,
                limit: limit,
                startkey: key,
                endkey: `${key}\ufff0`
            });
        } catch (err) {
            console.log(err);
        }
    };

    remove = async id => {
        try {
            //needs to get document first to ensure deletes latest revision
            return await this.DB.remove(await this.DB.get(id));
        } catch (err) {
            console.log(err);
        }
    };

    putAttachment = async (docID, attachmentID, data) => {
        try {
            console.log(`--> putting attachment: ${attachmentID}`);
            console.log("docID: " + docID);
            const docRev = (await this.DB.get(docID))._rev;
            console.log("docRev: " + docRev);
            await this.DB.putAttachment(docID, attachmentID, docRev, data, data.type);
        } catch (err) {
            if (err.status === 409) {
                console.log(`--> Retrying addAttachment(): ${attachmentID}`);
                await this.putAttachment(docID, attachmentID, data);
                // setTimeout(() =>{
                //
                // }, Math.floor(Math.random() * 50));
            }
        }
    };

    getAttachment = async (docID, attachmentID) => {
        try {
            return await this.DB.getAttachment(docID, attachmentID);
        } catch (err) {
            console.log(err);
        }
    };

    removeAttachment = async (docID, attachmentID) => {
        try {
            const doc = await this.DB.get(docID);
            await this.DB.removeAttachment(docID, attachmentID, doc._rev);
        } catch (err) {
            if (err.status === 409) {
                console.log(`--> Retrying removeAttachment(): ${attachmentID}`);
                await this.removeAttachment(docID, attachmentID);
            }
        }
    };
}

/**
 * Singleton by way of global "const"
 * source(s):
 *     https://k94n.com/es6-modules-single-instance-pattern
 *     https://github.com/k9ordon/es6-module-single-instance/tree/master/src
 */
export const pouchDB = new Pouch_Helper();