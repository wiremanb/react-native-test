import React, { Component } from "react";
import {observable, action} from 'mobx';
import {observer, inject} from 'mobx-react';
import  MapView, {Marker} from 'react-native-maps';
import {StyleSheet, View, AsyncStorage} from 'react-native'

const styles = StyleSheet.create({
	container: {
		position: "absolute",
		top: 0,
		bottom:0,
		left: 0,
		right: 0,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	map: {
		position: "absolute",
		top: 0,
		bottom:0,
		left: 0,
		right: 0,
	}
});

@inject("commonData")
@observer
export default class MapHelper extends Component {

	@observable value = "";
	@observable longitude = null;
	@observable latitude = null;
	@observable gotCoords = false;
	@observable isAnswered = this.props.navigation.state.params.answer ? true : false

	@action _updateOnDrag = (e) => {
		const coords = {
			latitude: e.nativeEvent.coordinate.latitude,
			longitude: e.nativeEvent.coordinate.longitude
		}
		this._updatePosition(coords);
	};

	_updatePosition = position => {

		this.props.navigation.state.params.onAnswer(this.props.navigation.state.params.index, position)
	}

	render() {
		if (this.isAnswered) {
			return (
				<View style={styles.container}>
					<MapView style={styles.map}
							 initialRegion={{
								 latitude: this.props.navigation.state.params.answer.latitude,
								 longitude: this.props.navigation.state.params.answer.longitude,
								 latitudeDelta: 0.0922,
								 longitudeDelta: 0.0421
							 }}
							 loadingIdicator={true}
							 scrollEnabled={true}>
						<Marker draggable
								coordinate={{
									latitude: this.props.navigation.state.params.answer.latitude,
									longitude: this.props.navigation.state.params.answer.longitude
								}}
								onDragEnd={async (e) => this._updateOnDrag(e)}
						/>
					</MapView>
				</View>
			);
		}
		else {
			return (
				<View style={styles.container}>
					<MapView style={styles.map}
							 initialRegion={{
								 latitude: this.props.commonData.latitude,
								 longitude: this.props.commonData.longitude,
								 latitudeDelta: 0.0922,
								 longitudeDelta: 0.0421
							 }}
							 loadingIdicator={true}
							 scrollEnabled={true}>
						<Marker draggable
								coordinate={{
									latitude: this.props.commonData.latitude,
									longitude: this.props.commonData.longitude
								}}
								onDragEnd={async (e) => this._updateOnDrag(e)}
						/>
					</MapView>
				</View>
			);
		}
	}
}

