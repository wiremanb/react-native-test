import React, { Component } from "react";
import {observable, toJS} from 'mobx';
import {observer} from 'mobx-react';
import { View} from 'react-native'

import DatePicker from 'react-native-datepicker';

@observer
export default class Form_Datepicker extends Component {
    @observable value = null;

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    };

    componentWillMount() {
        if(this.props.answer != null)
            this.value = new Date(this.props.answer);
    }

    handleChange = (date) => {
        date = new Date(date);
        date.setUTCHours(12,0,0,0);
        this.value = date;
        this.props.onAnswer(this.props.index, date);

    };

    render() {

        return (
			//<View style={{marginRight: 6}}>
                <DatePicker
                    style={{width: 250, backgroundColor:"white", marginRight: 20}}
                    date={this.value}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    onDateChange={this.handleChange}
                />
            //</View>
        );
    }
}