'use strict';

import React, { Component } from 'react';
import {Alert} from 'react-native';
import {NavigationActions} from 'react-navigation';
import styles from './styles.js';
import { Container, Header, Body, Title, Content, Form, Item, Input, Label, Button, Text } from 'native-base';
import {observable} from 'mobx';
import {inject, observer} from 'mobx-react';
import {GeolocationModule} from '../GPS/GPS.js';
// import {pouchDB} from "../../../Database/Pouch_Helper";

@inject("formDB")
@observer
class Login extends Component {
    @observable _username = "";
    @observable _password = "";
    @observable _authenticated = false;

    _buttonPress = async e => {
        try {
            this._authenticated = await this.props.formDB.auth(this._username, this._password);


            if (this._authenticated) {
                try {
                    this.props.formDB.loadClean();
                } catch(e) {
                    console.log("error loading db: " + e.message)
                }
                const resetAction = NavigationActions.reset({
                    index: 0,
                    key: null,
                    actions: [NavigationActions.navigate({routeName: 'MainMenu', params: {username: this._username}})],
                });
                this.props.navigation.dispatch(resetAction);
            }
            else {
                Alert.alert("Incorrect Login", "Username or password incorrect");
            }
        } catch(e) {
            console.log(e);
            Alert.alert("Incorrect Login", "Username or password incorrect");
        }
    };

    _handleTextInput(un_or_pw, txt) {
        if(un_or_pw === "username") {
            this._username = txt;
        }
        else {
            this._password = txt;
        }
    }

    render() {
        return (
            <Container>
				<GeolocationModule/>
                <Content>
                    <Form style={styles.loginForm}>
                        <Item fixedLabel>
                            <Input
                                autoCapitalize="none"
                                placeholder="Username"
                                onChangeText={text => {this._handleTextInput("username", text);}}
                            />
                        </Item>
                        <Item fixedLabel last>
                            <Input
                                secureTextEntry={true} 
                                placeholder="Password"
                                onChangeText={text => {this._handleTextInput("password", text);}}
                            />
                        </Item>
                    </Form>
                    <Button bordered primary style={styles.loginButton}
                        onPress={() => this._buttonPress()}>
                        <Text> Login </Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default Login;