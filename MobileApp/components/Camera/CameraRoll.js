'use strict';

import React, { Component} from 'react';
import {StyleSheet, Text, ScrollView, TouchableHighlight, View, Image, NativeModules} from 'react-native';
import {observable, toJS, computed} from 'mobx';
import {CameraRoll_Helper} from './CameraRoll';
import {observer} from 'mobx-react';
import {Container, Button } from 'native-base';
import Camera from 'react-native-camera';

@observer
export default class CameraRoll extends Component {
    @observable type = "CameraRoll";
    @observable images = []; // [ {attachmentID, blob, image} ... ]
    @observable currentImages = [];
    @observable options = { min: 0, max: null };
    @observable loaded = true;
    imageURI;
    camera = null;
    uri = null;

    constructor(props) {
        super(props);
    }

    @computed
    get answer() {
        return this.images.map(picture => {
            return { attachmentID: picture.attachmentID };
        });
    }

    componentWillMount() {
        this.type = this.props.navigation.state.params.type;

        if (this.props.navigation.state.params.values != null) {
            if (this.props.navigation.state.params.values.min != null) {
                this.options.min = this.props.navigation.state.params.values.min;
            }
            if (this.props.navigation.state.params.values.max != null) {
                this.options.max = this.props.navigation.state.params.values.max;
            }
        }

        if (this.props.navigation.state.params.answer !== null) {
            this.images = toJS(this.props.navigation.state.params.answer);
        }

        if (this.props.navigation.state.params.loadAttachment) {
            this.convertBlobsToImages();
        }
    }

    getTimeStamp() {
        const date = new Date();
        return `${date.getMonth() + 1}_${date.getDate()}_${date.getFullYear()} @ ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}:${date.getMilliseconds()}_${Math.floor(Math.random() * 1000)}`;
    }

    _changePage(type, imageURI) {
        this.imageURI = imageURI;
        this.type = type;
    }
    handlePics = (base64) => {
        // const base64 = fileReader.result;
        // console.log("FR.res: " + fileReader.result);
        const type = "image/jpeg";
        console.log("b64 image: " + base64);
        // const type = fileReader.result.substring(
        //     fileReader.result.lastIndexOf(":") + 1,
        //     fileReader.result.lastIndexOf(";")
        // );
        const blob = new Blob([base64], {type: "image/png"});
        console.log("bs: " + blob.size);
        const attachmentID = `image_${this.getTimeStamp()}`;
        this.images.push({
            attachmentID: attachmentID,
            blob: blob,
            image: base64
        });

        this.props.navigation.state.params.addNewAttachment({
            attachmentID: attachmentID,
            blob: blob
        });
        this.props.navigation.state.params.onAnswer(this.props.navigation.state.params.index, this.answer);
        // this.convertBlobsToImages();
    };

    uploadPictures = (imageUri) => {
        // const fileReader = new FileReader();

        // fileReader.onloadend = () =>
        // try {
        NativeModules.RNImageToBase64.getBase64String(imageUri, (err, base64) => this.handlePics(base64));
        // };

        // fileReader.readAsDataURL(imageUri);

        // this.props.navigation.state.params.onAnswer(this.props.navigation.state.params.index, this.answer, this.images);
        // this.convertBlobsToImages();
    };

    convertBlob = async (blob, index) => {
        if (!(blob instanceof Blob)) {
            console.log("Error: not of type Blob");
            return;
        }
        console.log("Blob: " + JSON.stringify(blob));
        NativeModules.RNImageToBase64.getBase64String(blob.data, (err, base64) => {
            this.images[index].image = `data:${blob.type.toString()};base64,${
                base64
                }`;
            // this.reRender();
        });
    };

    convertBlobsToImages = async () => {
        for (let i = 0; i < this.images.length; i++) {
            if (this.images[i].image == null && this.images[i].blob == null) {
                /* First Loaded */
                this.images[i].blob = await this.props.navigation.state.params.loadAttachment(
                    this.images[i].attachmentID
                );
            }
            if (this.images[i].image == null && this.images[i].blob != null) {
                await this.convertBlob(this.images[i].blob, i);
            }
        }
    };

    _takePicture = () => {
        const options = {};
        this.camera.capture({metadata: options})
            .then((data) => {
                console.log("data: " + JSON.stringify(data));
                this.uri = data.mediaUri;
                this.currentImages.push(this.uri);
                this.uploadPictures(this.uri);
                console.log("URI: " + this.uri)
                this._changePage("Preview", this.uri);
            })
            .catch(err => console.error(err));
    };

    _deleteImage = (imageIndex) => {
        this.props.navigation.state.params.removeOldAttachment(this.answer[this.images.indexOf(imageIndex)]);
        this.images.splice(this.images.indexOf(imageIndex), 1);
        this._changePage("CameraRoll", 0);
    }

    getPage = (type, imageURI) => {
        switch(type) {
            case "CameraRoll": {
                // console.log("Images: " + JSON.stringify(this.images));
                return (
                    <View style={styles.container}>
                        <ScrollView style={styles.container}>
                            <View style={styles.imageGrid}>
                                {
                                    this.images.map((image, index) => {
                                        console.log("CameraRoll: " + "data:image/jpeg;base64,"+image.image);
                                        console.log("Image Type: " + image.type);
                                        return (
                                            <TouchableHighlight
                                                key={index}
                                                onPress={() => this._changePage("Preview", "data:image/jpeg;base64,"+image.image)}>
                                                <Image style={styles.image} source={{uri: "data:image/jpeg;base64,"+image.image}}/>
                                            </TouchableHighlight>
                                        );
                                    })}
                            </View>
                        </ScrollView>
                        <Button
                            onPress={() => this._changePage("Camera", 0)}
                            style = {styles.button}>
                            <Text style = {styles.buttonText}>Add</Text>
                        </Button>
                        <Button
                            onPress={() => this.props.navigation.state.params.saveForm}
                            style = {styles.button}>
                            <Text style = {styles.buttonText}>Save</Text>
                        </Button>
                    </View>
                );
            }

            case "Camera": {
                //Alert.alert("CAMERA");
                return(
                    <Container key={this.props.navigation.state.params.index}>
                        <Camera
                            ref={(cam) => {
                                this.camera = cam;
                            }}
                            type={this.cameraType}
                            style={styles.preview}>
                            <View style={styles.buttonBar}>
                                <Button
                                    style={styles.button}
                                    onPress={this._takePicture}>
                                    <Text style={styles.buttonText}>Take</Text>
                                </Button>
                            </View>
                        </Camera>
                    </Container>
                );
            }

            case "Preview": {
                //Alert.alert("PREVIEW");
                return(
                    <View style = {styles.container}>
                        <Image style = {{width: '100%', height: '100%'}} source={{uri: imageURI}}/>
                        <View style = {styles.buttonBar}>
                            <Button
                                style = {styles.button}
                                onPress = {() => {
                                    // this.props.navigation.state.params.pictureTaken();
                                    this._changePage("CameraRoll", 0)
                                }}>
                                <Text style={styles.buttonText}>Keep</Text>
                            </Button>
                            <Button
                                style = {styles.button}
                                onPress = {() => {
                                    // this.props.navigation.state.params.pictureDeleted();
                                    this._deleteImage(imageURI);
                                }}>
                                <Text style={styles.buttonText}>Delete</Text>
                            </Button>
                        </View>
                    </View>
                );
            }
        }
    };

    render() {
        return(this.getPage(this.type, this.imageURI));
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    imageGrid: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
    image: {
        width: 200,
        height: 200,
        margin: 10,
        borderColor: "#000000",
        borderWidth: 1
    },
    buttonText: {
        color: "#000000",
        justifyContent: 'center',
        textAlign: 'center'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    button: {
        padding: 10,
        borderWidth: 1,
        borderColor: "#fff",
        margin: 5
    },
    buttonBar: {
        flexDirection: "row",
        position: "absolute",
        bottom: 25,
        right: 0,
        left: 0,
        justifyContent: "center"
    }
});