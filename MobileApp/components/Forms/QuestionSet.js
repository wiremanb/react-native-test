'use strict';

import React, { Component } from 'react';
import {StyleSheet} from 'react-native';
import {Button, Badge, Text, Icon} from 'native-base';
import {observable} from 'mobx';
import {observer, inject} from 'mobx-react';

@observer
export class QuestionSet extends Component {
    @observable title = "";
    @observable questionCount = 0;
    @observable questionsAnswered = 0;
    @observable questionSetComplete = false;

    constructor(props) {
        super(props);
        this.title = this.props.title;
        this.questionCount = this.props.questionCount;
        this.questionsAnswered = this.props.questionsAnswered;
        this.questionSetComplete = this.props.questionSetComplete;
    }


    render() {
        if(this.questionSetComplete) {
            return (
                <Button iconRight light bordered primary block
                        style={styles.questionSet}
                        onPress={() => this.props.onClick}>
                    <Icon name="arrow-forward"/>
                    <Text style={{fontSize: 20}}> {this.title} </Text>
                    <Badge width={27} style={styles.questionSetBadgeComplete}>
                        <Text>{this.questionCount}</Text>
                    </Badge>
                </Button>
            );
        }
        // else {
            return (
                <Button bordered primary block
                        style={styles.questionSet}
                        onPress={() => this.props.onClick}>
                    <Text style={{fontSize: 20}}> {this.title} </Text>
                    <Badge width={27} style={styles.questionSetBadgeNotComplete}>
                        <Text>{this.questionCount}</Text>
                    </Badge>
                </Button>
            );
        // }
    }
}

const styles = StyleSheet.create({
    questionSet: {
        fontSize: 20,
        fontWeight: 'bold',
        //marginBottom: 20,
    },
    questionSetBadgeNotComplete: {
        fontSize: 20,
        justifyContent: 'center',
        alignItems: 'center',
		flexWrap:"nowrap",
        left: 10,
        top: 1,
        // color: 'red',
    },
    questionSetBadgeComplete: {
        fontSize: 20,
        justifyContent: 'center',
        alignItems: 'center',
		flexWrap:"nowrap",
        left: 10,
        top: 1,
        // color: 'green',
    }
});