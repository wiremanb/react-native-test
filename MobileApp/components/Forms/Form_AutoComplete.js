import React, { Component } from 'react';
import { View, Dimensions } from 'react-native'
import { Container, Header, Content, Card, CardItem, Text, Icon, Right, ListItem, Button} from 'native-base';
import LineEdit from './Form_LineEdit.js'
import {observable, toJS, action} from 'mobx';
import {observer} from 'mobx-react';
var {height, displayWidth} = Dimensions.get('window');

@observer
export default class AutoComplete extends Component {
    @observable displayText = '';
    @observable displayItems = null;
    constructor(props){
        super(props);
        this._handleChange = this._handleChange.bind(this);
        // this._buttonPress = this._buttonPress.bind(this)
    };

    componentWillMount() {
        if(this.props.answer != null)
            this.displayText = this.props.answer;

        console.log("WOOT: " + this.displayText);
    }

    @action _handleChange (event) {
        //If the change is them deleting all input, remove suggestions
        if(event.nativeEvent.text == '') {
            console.log("Setting displayItems to null");
            this.displayItems = null;
            this.displayText = '';
            return;
        }

        // this.props.onAnswer(this.props.index, this.displayText);

        var updatedList = this.props.values;

        this.displayText = event.nativeEvent.text;

        // this.props.onAnswer(this.props.index, this.displayText);

        updatedList = updatedList.filter(function(item) {
            return item.toLowerCase().search(
                event.nativeEvent.text.toLowerCase()) !== -1;
        });
        this.displayItems = updatedList;
        console.log("DI: " + this.displayItems);
    }

    @action _handleFocus= () => {
        this.displayItems = this.props.values;
    };

    @action _buttonPress = (text) => {
        this.displayText = text;
        this.displayItems = null;
        this.props.onAnswer(this.props.index, this.displayText);
        console.log("Button DT: " + this.displayText);
    };

    @action _handleBlur = () => {
        this.displayItems = null;
    };

    render() {
        console.log("(Render) DT: " + this.displayText);
        console.log("(Render) this.props.values: " + this.props.values);
        var displayHeight = height/15;
        return (
                <View>
                    <LineEdit
                        style={{fontSize: 20, backgroundColor:"white"}}
                        placeholder={this.props.placeholder}
                        onUpdateInput={this._handleChange}
                        value={this.displayText}
                        answer = {this.props.answer != null ? this.props.answer : this.displayText}
                        onAnswer={this.props.onAnswer}
                        index={this.props.index}
                        onFocus={this._handleFocus}
                        type={this.props.type}
                        // onBlur={this._handleBlur}
                    />

                        <Card
                            style={{ shadowOffset:{width:0, height:0}, borderBottomWidth: 0, borderTopWidth: 0}}
                            dataArray={toJS(this.displayItems)}
                            renderRow={data =>
                                <CardItem
                                    key={data}
                                    button onPress={() => this._buttonPress(data)}>
                                    <Text>{data}</Text>
                                </CardItem>}/>

                </View>
        );
    }
}