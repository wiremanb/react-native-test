'use strict';

import React, { Component, Platform } from "react";
import { Text, View, Modal } from 'react-native'
import AutoComplete from "./Form_AutoComplete";
import LineEdit from './Form_LineEdit';
import Form_Datepicker from './Form_Datepicker';
import {Camera_Component} from '../Camera/Camera_Component';
import {observable} from 'mobx';
import {observer, inject} from 'mobx-react';
import Map from './Form_Map';

@inject("commonData")
@observer
export class Question extends Component {
    @observable modalVisible = false;
    generateQuestion = () => {
        switch (this.props.type) {
            case "Line Edit":
                return (

                    <LineEdit
                        multiline={false}
                        placeholder={this.props.placeholder}
                        answer = {this.props.answer}
                        onAnswer={this.props.onAnswer}
                        index={this.props.index}

                    />
                );
            case "Pick List":
                // console.log("AC: " + this.props.values);
                return (
                        <AutoComplete
                            placeholder={this.props.placeholder}
                            values={this.props.values}
                            answer = {this.props.answer}
                            onAnswer={this.props.onAnswer}
                            index={this.props.index}
                            type={"autocomplete"}
                        />
                );

            case "YesNo":// Reference.QuestionTypes.YesNo:
                return (
                    <AutoComplete
                        placeholder={this.props.placeholder}
                        values={["Yes", "No"]}
                        answer = {this.props.answer}
                        onAnswer={this.props.onAnswer}
                        index={this.props.index}
                        type={"autocomplete"}
                    />
                );

            case "Multi-line Edit":// Reference.QuestionTypes.MultiLineEdit:
                return (
                        <LineEdit
                            multiline={true}
                            placeholder={this.props.placeholder}
                            answer = {this.props.answer}
                            onAnswer={this.props.onAnswer}
                            index={this.props.index}
                        />
                );

            case "Date":// Reference.QuestionTypes.Date:
                return (
                        <Form_Datepicker
                            placeholder="Select a date"
                            answer = {this.props.answer}
                            onAnswer={this.props.onAnswer}
                            index={this.props.index}
                        />
                );

            case "GPS":// Reference.QuestionTypes.GPS:
<<<<<<< HEAD

				 return (
				     	<Map
							navigation={this.props.navigation}
							answer={this.props.answer}
							onAnswer={this.props.onAnswer}
							index={this.props.index}
						/>
				 );

			//<label>Some type of GPS coord</label>;
=======
                return (
                    <View style={{margin: 20, zIndex: 0, position: 'relative'}}>
                        <Text style={{fontSize: 15, marginBottom: 5}}>{"Lat: " + coordinates.latitude}</Text>
                        <Text style={{fontSize: 15}}>{"Lon: " + coordinates.longitude}</Text>
                        <Button primary bordered block
                                style={{marginTop: 5}}
                                key={Math.random()}
                                onPress={() => {
                                    this.props.onAnswer(this.props.index, {"latitude": coordinates.latitude, "longitude": coordinates.longitude});
                                    this.props.navigation.navigate("Maps")
                                }}>
                            <Text>{"Show Map"}</Text>
                        </Button>
                    </View>
                ); //<label>Some type of GPS coord</label>;
>>>>>>> remotes/origin/spectrum-db

            case "Picture":// Reference.QuestionTypes.Picture:
                return (
                        <Camera_Component
                            navigate={this.props.navigation}
                            title={this.props.title}
                            onAnswer={this.props.onAnswer}
                            addNewAttachment={this.props.addNewAttachment}
                            removeOldAttachment={this.props.removeOldAttachment}
                            index={this.props.index}
                            values={this.props.values}
                            docID={this.props.docID}
                            formTitle={this.props.formTitle}
                            answer={this.props.answer}
                            loadAttachment={this.props.loadAttachment}
                            saveForm={this.props.saveForm}
                        />
                );

            default:
                return null; //<label>There is no component for this yet</label>;
        }
    };
    //<View style={{margin: 10, borderWidth: 0.5, borderColor: 'black', flex: 1, paddingBottom: 10, flexDirection: "row", alignItems:"center", justifyContent: 'space-between'}}>
    render() {
        return (
            <View style={viewStyle}>
                <Text style={{fontSize: 20 ,fontWeight: 'bold', maxWidth: "50%" }}>{this.props.questionText}</Text>
                {this.generateQuestion()}
            </View>
        );
    }
}
	const viewStyle = {
		elevation: 2,
		margin: 10,
		shadowColor: '#000',
		shadowOffset: {width: 10, height: 10},
		shadowOpacity: 0.8,
		shadowRadius: 2,
		backgroundColor:"#eee",
		flex: 1,
		paddingLeft: 10,
		paddingTop: 10,
		paddingBottom: 10,
		flexDirection: "row",
		justifyContent: 'space-between'
	}