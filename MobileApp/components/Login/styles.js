const React = require("react-native");

const { StyleSheet } = React;

export default {
  container: {
    backgroundColor: "#FFF"
  },
  loginButton: {
    marginTop: 10,
    flexGrow: 1,
    alignSelf: "center",
    alignItems: "center"
  },
  loginForm: {
    marginTop: 50,
    flexGrow: 1,
    alignSelf: "center",
    alignItems: "center"
  },
  image: {
    justifyContent: 'center',
    alignItems: 'center',
  }
};