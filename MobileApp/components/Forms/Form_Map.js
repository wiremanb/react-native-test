import React, {Component} from "react";
import {Text, View} from "react-native";
import {action, observable} from "mobx";
import {observer, inject} from 'mobx-react';
import { Button, Icon } from "native-base";

@inject("commonData")
@observer
export default class Map extends Component {

	@action _handleNavigation() {
		const coords = {
			latitude: this.props.commonData.latitude,
			longitude: this.props.commonData.longitude
		}

		//If we haven't answered, answer with the current location
		if(this.props.answer == null)
		{
			this.props.onAnswer(this.props.index, coords)

		}

		this.props.navigation.navigate(
			"Maps",
			{
				onAnswer: this.props.onAnswer,
				index: this.props.index,
				answer: this.props.answer
			}
		);
	}

		render() {
			return (
				<View style={{marginRight: 20, zIndex: 0, position: 'relative'}}>
					{/*<Text style={{fontSize: 15, marginBottom: 5, width: 250}}>{"Longitude: " + (this.props.answer != null ? this.props.answer.longitude : "")}</Text>*/}
					{/*<Text style={{fontSize: 15, width: 250}}>{"Latitude: " + (this.props.answer != null ?  this.props.answer.latitude : "")}</Text>*/}
					<View style={{flex: 1}}>

						<Button bordered block
							onPress={() => this._handleNavigation()}
							style={{width: 250, backgroundColor:"white"}}
						>
							<Icon name="pin" style={{color: "red"}}/>
							<Text style={{color: "#363738", fontSize: 20}}>
								{this.props.answer != null ? "lat/long(" + Math.floor(this.props.answer.latitude) + "," + Math.floor(this.props.answer.longitude) + ")"  : "Get Coords" }</Text>
						</Button>
					</View>
				</View>
			);
		}
}


